#! /usr/bin/env bash 
WEBDIR=$HOME/.webscraper
# function declaration to be called upon later!
function webscrape () {

printf "Specify your web page\n: "
read PAGE
wait 1
printf "Are you sure you want to scrape this page?\ny\nN\n: "
read PGCHOICE
wait 1
if [ $PGCHOICE == 'y' ]
then
	cd $WEBDIR && wget $PAGE
elif [ $PGCHOICE =='N' ]
then
	echo "Okay, Goodbye!"
else
	echo "CRITICAL ERROR"
	exit
fi
}



function webscrape-recursive () {
# line 6 asks the user what website they plan on scrapping.
printf "Specify your desired website\n: "
read SITE
wait 1
# line 10 is supposed to ask the user they're want to scrape their website of choice.
printf "Are you sure you what to scrape this website?\ny\nN\n: "
read CHOICE

# lines 14 to 26 are supposed to handle the users choices using if, elif and else statements.
if [ $CHOICE == 'y' ]
then
# line 17 recursively downloads the website to the .webscraper directory
	cd $WEBDIR && wget --recursive $SITE
elif [ $CHOICE == 'N' ]
then
	echo "Okay, Goodbye!"
	wait 1
	exit
else
	echo "CRITICAL ERROR!!!"
	exit
fi }

function scrape-options () {

printf "Do you want to scrape an entire website or a single page?\n(1 - One-Page\n(2 - Entire-Site\n: "
read SCRPOPT
if [ $SCRPOPT = 1 ]
then
	webscrape
elif [ $SCRPOPT = 2 ]
then
	webscrape-recursive
else
	echo "ERROR, selected option doesn't exist!" 
	exit
fi }


# Checks whether the script's downloads directory exists. If not, then the script will create one.

if [ -d "$WEBDIR" ] 
then
	scrape-options
elif [ ! -d "$WEBDIR" ]
then
	mkdir $WEBDIR && scrape-options
else
exit
fi 